import {RequestParams} from './interface.ts';
import {getUserById} from '../../../../../core/db-transactions/User.ts';

export function handler (req, res, next) {
    var requestParams: any = req.body;
    getUserById(req.cookies.uid).then(function (me) {
        res.status(200);
        res.json(me);
        res.end();
    }).catch(function (err) {
        res.status(500);
        res.json(err);
        res.end();
    });
}