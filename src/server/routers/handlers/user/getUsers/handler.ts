import {RequestParams} from './interface.ts';
import {getUsers} from '../../../../../core/db-transactions/User.ts';

export function handler (req, res, next) {
    var requestParams: any = req.body;
    getUsers().then(function (users) {
        res.status(200);
        res.json(users);
        res.end();
    }).catch(function (err) {
        res.status(500);
        res.json(err);
        res.end();
    });
}