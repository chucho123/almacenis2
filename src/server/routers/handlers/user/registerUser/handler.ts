import {RequestParams} from './interface.ts';
import {createUser} from '../../../../../core/db-transactions/User.ts';

export function handler (req, res, next) {
    var requestParams: any = req.body;
    createUser(requestParams).then(function (user) {
        res.status(200);
        res.json(user);
        res.end();
    }).catch(function (err) {
        res.status(500);
        res.json(err);
        res.end();
    });
}