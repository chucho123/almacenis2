import {User as UserModel } from '../db-models/User.ts';
import {MongoModel} from '../classes/MongoModel.ts';
import * as q from 'q';
import config from '../../settings/index.ts';

export function createUser (userData: UserModel) {
    var deferred = q.defer();
    var model = new MongoModel(config.dbConfig.models.user.name);
    model.insert(userData).then(function (user) {
        deferred.resolve(user);
    }).catch(deferred.reject);
    return deferred.promise;
}

export function getUsers () {
    var deferred = q.defer();
    var model = new MongoModel(config.dbConfig.models.user.name);
    model.findAll({}).then(function (users) {
        deferred.resolve(users);
    }).catch(deferred.reject);
    return deferred.promise;
}

export function verifyUserCredentials (email: string, password: string) {
    var deferred = q.defer();
    var model = new MongoModel(config.dbConfig.models.user.name);
    model.findOne({
        email: email,
        password: password
    }).then(function (user) {
        if (!user) {
            deferred.reject({
                status: 401,
                details: 'Invalid credentials'
            });
            return;
        }
        deferred.resolve(user);
    }).catch(deferred.reject);
    return deferred.promise;
}

export function getUserById (userId: string) {
    var deferred = q.defer();
    var model = new MongoModel(config.dbConfig.models.user.name);
    model.findById(userId).then(function (user) {
        if (!user) {
            deferred.reject({
                status: 404,
                detail: 'Invalid user id'
            });
            return;
        }
        deferred.resolve(user);
    }).catch(deferred.reject);
    return deferred.promise;
}