import {Component, OnInit} from '@angular/core';
import {Product} from '../../../../../../../../core/db-models/Product.ts';
import {Provider} from '../../../../../../../../core/db-models/Provider.ts';
import {Lot} from '../../../../../../../../core/db-models/Lot.ts';
import {Resources} from '../../services/Resources.ts';


@Component({
	styles: [require('./style.styl').toString()],
	template: require('./template.jade')(),
})

export class DetailProductLotComponent implements OnInit {
	
	// Attributes
		providers: Provider[];
		products: Product [];
		lots: Lot[];
		currentProvider: Provider;
		currentProduct: Product;
		currentLot: Lot;
		resources: Resources;
		

	// Methods
		constructor (resources: Resources) {
			this.currentProvider = null;
			this.currentProduct = null;
			this.currentLot = null;
			this.providers = [];
			this.resources = resources;
			this.products = [];
			
		}

		ngOnInit () {
			this.resources.getProviders({
				urlParams: {},
				data: {} 
			}).subscribe((resp) => {
				this.providers = resp;
				console.log(this.providers);
			})
		}

		getProducts () {
			setTimeout(() => {
				this.resources.getProviderProducts({
					urlParams: { providerId: this.currentProvider.id },
					data: {}
				}).subscribe((resp: any) => {
					this.products = resp;
					console.log(resp);
				})
			}, 10);
		}

		getProductLots () {
			setTimeout(() => {
				this.resources.getProductLots({
					urlParams: {productId: this.currentProduct.id},
					data: {}
				}).subscribe((resp: any) => {
					this.lots = resp;
					console.log(resp);
				})
			},10);
		}



		



}
