import {Component} from '@angular/core';
import {Response, Http} from '@angular/http';
import {Resources} from './services/Resources.ts';

interface IAction {
	name: string;
	path?: string;
}

interface IModule {
	name: string;
	actions: IAction [];
}

@Component({
	selector: 'app',
	styles: [require('./app.styl').toString()],
	template: require('./app.jade')(),
})
export class AppComponent {
	
	// Attributes
		modules: IModule [];
		resources: Resources;
		$http: Http;
		username: string;

	// Methods
		constructor (resources: Resources, $http: Http) {
			this.$http = $http;
			this.resources = resources;
			this.username = '';
			var mods = {
				providers: {
					name: 'Proveedores',
					actions: [{
						name: 'Registrar',
						path: '/register-provider'
					},
					{
						name: 'Listar',
						path: '/list-provider'
					},
					{
						name: 'Ver detalles',
						path: '/detail-provider'
					}]
				},
				products: {
					name: 'Productos',
					actions: [{
						name: 'Registrar',
						path: '/register-product'
					},
					{
						name: 'Listar',
						path: '/list-product'
					},
					{
						name: 'Ver detalles',
						path: '/detail-product'
					}]
				},
				lots: {
					name: 'Lotes',
					actions: [{
						name: 'Registrar',
						path: '/register-product-lot'
					},
					{
						name: 'Listar',
						path: '/list-product-lot'
					},
					{
						name: 'Ver detalles',
						path: '/detail-lot'
					}]
				},
				location: {
					name: 'Ubicación',
					actions: [{
						name: 'Ver',
						path: '/view-location'
					}]
				},
				order: {
					name: 'Pedidos',
					actions: [{
						name: 'Crear orden',
						path: '/create-order'
					}, {
						name: 'Ver ordenes',
						path: '/view-orders'
					}]
				}
			}
			this.modules = [];
			this.resources.getMyData().subscribe(
				(myUser) => {
					this.username = myUser.name;
					switch (myUser.role) {
						case 'admin':
							this.modules.push(mods.providers);
							this.modules.push(mods.products);
							this.modules.push(mods.lots);
							this.modules.push(mods.location);
							this.modules.push(mods.order);
						break;
						case 'seller':
							this.modules.push(mods.lots);
							this.modules.push(mods.location);
							this.modules.push(mods.order);
						break;
						case 'incomes': 
							this.modules.push(mods.products);
							this.modules.push(mods.lots);
							this.modules.push(mods.location);
						break;
					}
				}
			)
			/*this.modules = [{
				name: 'Proveedores',
				actions: [{
					name: 'Registrar',
					path: '/register-provider'
				},
				{
					name: 'Listar',
					path: '/list-provider'
				},
				{
					name: 'Ver detalles',
					path: '/detail-provider'
				}]
			},
			{
				name: 'Productos',
				actions: [{
					name: 'Registrar',
					path: '/register-product'
				},
				{
					name: 'Listar',
					path: '/list-product'
				},
				{
					name: 'Ver detalles',
					path: '/detail-product'
				}]
			},
			{
				name: 'Lotes',
				actions: [{
					name: 'Registrar',
					path: '/register-product-lot'
				},
				{
					name: 'Listar',
					path: '/list-product-lot'
				},
				{
					name: 'Ver detalles',
					path: '/detail-lot'
				}]
			}]*/
		}

}
