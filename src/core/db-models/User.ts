import {BaseModel} from './BaseModel.ts';

export interface User extends BaseModel {
    name: string;
    role: string;
    email: string;
    password: string;
}