import {ExpressServer} from '../core/classes/ExpressServer.ts';
import {connectDatabase} from '../core/services/connectDatabase.ts';
import {getUserById} from '../core/db-transactions/User.ts';
import {handlersRouter, validationRouter} from './routers/index.ts'; 
import config from '../settings/index.ts';
import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';

// Define Server
var server  = new ExpressServer();

// Setup middleware
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(cookieParser());

// Setup Server
server.server.use('/statics', express.static(path.join(process.cwd(), 'src/server/statics')));

// Setup routers
server.addRouter('/api', validationRouter.router);
server.addRouter('/api', handlersRouter.router);

// Setup views
server.server.get('/users', function (req, res, next) {
    if (!req.cookies.uid) {
        res.redirect('/access');
        return;
    }
    getUserById(req.cookies.uid).then(function (user: any) {
        if (user.role != 'admin') {
            res.redirect('/dashboard');
            return;
        }
        var viewPath = path.join(process.cwd(), 'src/server/statics/webapps/users/src/index.html'); 
        res.sendFile(viewPath);
    })
});

server.server.get('/dashboard', function (req, res, next) {
    if (!req.cookies.uid) {
        res.redirect('/access');
        return;
    }
    var viewPath = path.join(process.cwd(), 'src/server/statics/webapps/dashboard/src/index.html'); 
    res.sendFile(viewPath);
});

server.server.get('/access', function (req, res, next) {
    if (req.cookies.uid) {
        res.redirect('/dashboard');
        return;
    }
    var viewPath = path.join(process.cwd(), 'src/server/statics/webapps/access/src/index.html'); 
    res.sendFile(viewPath);
});

// Start Server
connectDatabase(config.dbConfig.url).then(function () {
    console.log('Connected to database..');
    server.listen(config.server.port).then(function () {
        console.log('Server up at port '+config.server.port);
    }).catch(function (err) {
        console.log('Error starting server..', err);
    })
}).catch(function (err) {
    console.log('Error connecting to database', err);
});