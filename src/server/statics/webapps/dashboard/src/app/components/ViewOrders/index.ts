import {Component} from '@angular/core';

@Component({
	styles: [require('./style.styl').toString()],
	template: require('./template.jade')(),
})
export class ViewOrdersComponent {
	
	// Attributes
		showLots: boolean;

	// Methods
		constructor () {
			this.showLots = false;
		}
		showLotsAction () {
			this.showLots = true;
		} 

}
