import {RequestParams} from './interface.ts';
import {verifyUserCredentials} from '../../../../../core/db-transactions/User.ts';

export function handler (req, res, next) {
    var requestParams: any = req.body;
    verifyUserCredentials(req.body.email, req.body.password).then(function (user: any) {
        res.status(200);
        res.cookie('uid', user.id);
        res.json(user);
        res.end();
    }).catch(function (err) {
        res.status(err.status || 500);
        res.json(err);
        res.end();
    });
}