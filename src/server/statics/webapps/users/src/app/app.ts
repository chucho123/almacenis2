import {Component, OnInit} from '@angular/core';
import {Response, Http} from '@angular/http';
import config from '../../../../../../settings/index.ts';
import {User as UserModel} from '../../../../../../core/db-models/User.ts';

@Component({
	selector: 'app',
	styles: [require('./app.styl').toString()],
	template: require('./app.jade')(),
})
export class AppComponent implements OnInit {

	// Attributes
		$http: Http;
		domain: string;
		users: UserModel[];
		currentUser: UserModel;

	// Methods
		constructor ($http: Http) {
			this.$http = $http;
			this.users = [];
			this.domain = 'http://localhost:3000';
			this.currentUser = {
				email: '',
				name: '',
				password: '',
				role: ''
			}
		}
		ngOnInit () {
			this.loadUsers();
		}
		loadUsers () {
			this.request('getUsers', {}, {}).subscribe(
				(resp) => {
					console.log(resp);
					this.users = resp;
				},
				(resp) => { console.warn(resp) }
			)
		}
		registerUser () {
			this.request('registerUser', {}, this.currentUser).subscribe(
				(resp) => {
					console.log(resp);
					this.loadUsers();
					alert('User registered');
				},
				(resp) => { console.warn(resp) }
			)
		}
		request (serviceName: string, urlParams: any, data?: any) {
			var service = config.apiServices[serviceName];
			var response = null;
			var url = this.domain+service.url;
			if (data) {
                response = this.$http[service.method.toLowerCase()](url, data);
            }
            else {
                response = this.$http[service.method.toLowerCase()](url);
            }
			return response.map((data:Response) => data.json());
		}

}
