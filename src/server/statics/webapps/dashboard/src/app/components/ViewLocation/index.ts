import {Component, OnInit} from '@angular/core';
declare var THREE:any;

@Component({
	styles: [require('./style.styl').toString()],
	template: require('./template.jade')(),
})
export class ViewLocationComponent implements OnInit{
	
	// Attributes
		scene: any;
		stands: any[];

	// Methods
		constructor () {
			this.scene = null;
			this.stands = [];
		}
		ngOnInit () {
			var container = document.getElementById('canvas-container');
			var scene = new THREE.Scene();
			this.scene = scene;
			var camera = new THREE.PerspectiveCamera(75,container.offsetWidth/container.offsetHeight, 1,10000);
			var renderer = new THREE.WebGLRenderer();
			var controls = new THREE.OrbitControls( camera, renderer.domElement );
			// Setting up renderer
			renderer.setSize(container.offsetWidth, container.offsetHeight);
    		container.appendChild(renderer.domElement);

			// Setting up camera
			var dirLighta = new THREE.DirectionalLight(0xffffff, 1);
			dirLighta.position.set(0, 50, 80);
			scene.add(dirLighta);
			camera.position.z = 100;
			var dirLightb = new THREE.DirectionalLight(0xffffff, 1);
			dirLightb.position.set(0, -50, 80);
			scene.add(dirLightb);
			camera.position.z = 100;

			// Setting up floor
			var geometry = new THREE.BoxGeometry(100, 100, 1);
			var material = new THREE.MeshBasicMaterial({color: 0x333333, wireframe: false});
			var cube = new THREE.Mesh(geometry, material);
			scene.add(cube);
			console.log(scene);
			
			// Setting random values
			var nStands = Math.ceil( Math.random()*5 );
			var nRows = 2+Math.round( Math.random()*5 );
			var nCols = 2+Math.round( Math.random()*5 );

			this.setupStands(3, 3, 4);
			this.setupLot(1,2,1);

			// Rendering
			function render() {
				requestAnimationFrame(render);
				renderer.render(scene, camera);
			}
			render();

			//
		}
		setupStands (nStands, nRows, nCols) {
			var squareSize = 10;
			//var nStands = 3;
			//var nRows = 3;
			//var nCols = 4;
			for (var s=0; s<nStands; s++) {

				// Setting up x
				for (var r=0; r<nRows+1; r++) {
					for (var c=0; c<nCols; c++) {
						var geometry = new THREE.BoxGeometry(squareSize, squareSize, 1);
						var material = new THREE.MeshPhongMaterial({color: 0x9e3300, wireframe: false});
						var square = new THREE.Mesh(geometry, material);
						square.position.x = c*squareSize-squareSize*nRows/2;
						square.position.y = s*squareSize*2-nStands*squareSize/2;
						square.position.z = r*squareSize+1;
						this.scene.add(square);
					}
				}

				// Setting up y
				for (var c=0; c<nCols+1; c++) {
					for (var r=0; r<nRows; r++) {
						var geometry = new THREE.BoxGeometry(1, squareSize, squareSize);
						var material = new THREE.MeshPhongMaterial({color: 0x9e3300, wireframe: false});
						var square = new THREE.Mesh(geometry, material);
						square.position.x = c*squareSize-squareSize*nRows/2-squareSize/2;
						square.position.y = s*squareSize*2-nStands*squareSize/2;
						square.position.z = r*squareSize+1+squareSize/2;
						this.scene.add(square);
					}
				}

			}
		}

		setupLot (stand, row, col) {
			var squareSize = 10;
			var geometry = new THREE.BoxGeometry(squareSize, squareSize, squareSize);
			var material = new THREE.MeshPhongMaterial({color: 0xEEEEEE, wireframe: false});
			var square = new THREE.Mesh(geometry, material);
			square.position.x = col*squareSize-squareSize/2;
			square.position.y = stand*squareSize-squareSize/2;
			square.position.z = row*squareSize-squareSize/2+1;
			this.scene.add(square);
		}

}
