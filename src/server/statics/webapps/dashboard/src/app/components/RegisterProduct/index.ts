import {Component, OnInit} from '@angular/core';
import {Product} from '../../../../../../../../core/db-models/Product.ts';
import {Resources} from '../../services/Resources.ts';
import {Provider} from '../../../../../../../../core/db-models/Provider.ts';
import {Lot} from '../../../../../../../../core/db-models/Lot.ts';
@Component({
	styles: [require('./style.styl').toString()],
	template: require('./template.jade')(),
})
export class RegisterProductComponent implements OnInit {
	
	// Attributes
		product: Product;
		resources: Resources;
		currentProvider: Provider;
		providers: Provider[];

		//providers:Provider[]
	// Methods
		constructor (resources: Resources) {
			this.product = {
				name: '',
				providerId: ''
			}
			this.resources = resources;
			this.currentProvider = null;
			this.providers = [];
			this.resources = resources;
			
		}
		ngOnInit () {
			this.resources.getProviders({
				urlParams: {},
				data: {} 
			}).subscribe((resp) => {
				this.providers = resp;
				console.log(this.providers);
			})
		}
		submitRegister () {
			this.product.providerId = this.currentProvider.id;
			//console.log(this.product);
			this.resources.registerProviderProduct({
				urlParams: {
					providerId: this.product.providerId,
				},
				data: {
					name : this.product.name
				}
			}).subscribe((resp) => {
				console.log(resp);
				alert('Registrado!');
			})
		}
		
}