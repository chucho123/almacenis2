import {Component, OnInit} from '@angular/core';
import {Response, Http} from '@angular/http';
import config from '../../../../../../settings/index.ts';

@Component({
	selector: 'app',
	styles: [require('./app.styl').toString()],
	template: require('./app.jade')(),
})
export class AppComponent implements OnInit {
	
	// Attributes
		credentials: { email: string; password: string };
		domain: string;
		$http: Http;

	// Methods
		constructor ($http: Http) {
			this.$http = $http;
			this.domain = 'http://localhost:3000';
			this.credentials = {
				email: '',
				password: ''
			}
		}
		ngOnInit () {}
		submitCredentials () {
			console.log(this.credentials);
			//validateCredentials
			this.request('validateCredentials', {}, this.credentials).subscribe(
				(resp) => { 
					alert('Welcome!');
					window.location.replace('/dashboard');
				 },
				(resp) => {
					console.warn(resp);
					alert('Invalid credentials!');
					this.credentials.password = '';
				}
			)
		}
		request (serviceName: string, urlParams: any, data?: any) {
			var service = config.apiServices[serviceName];
			var response = null;
			var url = this.domain+service.url;
			if (data) {
                response = this.$http[service.method.toLowerCase()](url, data);
            }
            else {
                response = this.$http[service.method.toLowerCase()](url);
            }
			return response.map((data:Response) => data.json());
		}

}